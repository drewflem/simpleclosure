simpleClosure
=============

A simple Python script for minifying all javascript files in a root folder, including all sub folders.
-----

This script was built to facilitate the compression of a full Javascript project mostly automatically.

All configuration options are built into the simpleClosure.py file. This is the script that, when run, will crawl through recursively a root folder, hunting for all files with the *.js extension, and build a buildFile.bat that, when run, will use Google Closure to Minify all files.

Step 1: Install Python 2.7
	http://www.python.org/download/

Step 2: Install Closure
	http://closure-compiler.googlecode.com/files/compiler-latest.zip
	This downloads the latest Closure compiler. Install it, and note where it installs
	Its easiest if the path you install to includes no spaces

Step 3: Edit the simpleClosure.py file for your Closure install

Step 4: Copy your entire project to a designated Deploy folder

Step 5: Edit the simpleClosure.py file to specify your JavaScript project's new Deploy folder

Step 6: Run simpleClosure.py

Step 7: Run the resulting buildFile.bat file to minify your code

Please Note - this will OVERWRITE your javascript files in your Deploy folder/subfolders. Make absolutely sure you are not running this script on your original source files!