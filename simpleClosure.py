import os
import sys

# Change this line to correspond with JS Project's root folder
rootdir = "C:\Projects\Analytics\AssessmentMin"

# File that will be generated to Minify all .js files
outfileName = os.path.join(rootdir, "buildFile.bat")
print "Build File can be found here: " + outfileName

# Change this line to reflect your desired compilation level, which
#    handles how much to compress your files.
# WHITESPACE_ONLY - Removes comments, line breaks, unnecessary spaces, whitespace
# SIMPLE_OPTIMIZATIONS - WHITESPACE_ONLY + also performs optimizations within
#    expressions and functions, including renaming local variables and
#    function parameters to shorter names
# ADVANCED_OPTIMIZATIONS - full compression, please refer to the online
#    documentation for more information
compLevel = "SIMPLE_OPTIMIZATIONS"

for root, subFolders, files in os.walk(rootdir):
    with open( outfileName, 'a+' ) as writeOut:
        for fullFileName in files:
            fileName, fileExtension = os.path.splitext(fullFileName)
            
            # Change or add to this line to restrict files to Minify
            if fileExtension == ".js":
                inFile = os.path.join(root, fullFileName)
                
                # Temp file name since Closure won't overwrite original file automatically
                outFileName = fileName + "Min" + fileExtension
                outFile = os.path.join(root, outFileName)

                # Change this line to correspond with your Closure location
                minifyIt = "java -jar c:\Closure\compiler.jar --compilation_level " + compLevel + " --js_output_file " + outFile + " --js " + inFile
                thenDelete = "del " + inFile
                thenRename = "rename " + outFile + " " + fullFileName

                # Files will be Minified to new file, then original file deleted,
                #    then minified file rename to original file name
                writeOut.write(minifyIt + '\n')
                writeOut.write(thenDelete + '\n')
                writeOut.write(thenRename + '\n')